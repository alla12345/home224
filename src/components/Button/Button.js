import './Button.scss';
import React, {Component} from 'react';

export default class Button extends  Component {
  state = {
    clickButton: 0,
    isClick: false,
  }

  handleClick = () => {
    const { text } = this.props;
    // this.setState((state) => {
    //   return {clickButton: state.clickButton + 1};
    // });
    //если есть зависимость

    this.setState((prevState) => ({     
      clickButton: prevState.clickButton + 1,
    }));
    // this.setState({clickButton: this.state.clickButton + 1});
    console.log(text);
  }
  isEven = (value) => {
    return !(value % 2);
  }

  render() {
    const { text } = this.props;
    const { clickButton } = this.state;
    const buttonClass =  this.isEven(clickButton) ? 'button button__green' : 'button';
   
    return(
      <button className={buttonClass} onClick={this.handleClick}>
        {text}({clickButton})
      </button>
    )
  }
}
