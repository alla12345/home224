import './OvalHolder.scss';
import React, {Component} from 'react';

// export default class OvalHolder extends Component {

//   render() {
//     const { color, withHover, index } = this.props;
    
//     return (
//       <div className={`oval-holder oval-holder_${color} ${withHover ? 'oval-holder_is-opacity' : ''}`}>
        
//       </div>
//     );
//   }
// }

export default class OvalHolder extends Component {
  
  // state = {
  //   isClick: false,
  // }

  // handleClick = () => {
  //   const { index } = this.props;
  //   this.setState({
  //     isClick: true,
  //   });
  //   console.log(index);
  // }

  handleClick = () => {
    const { onClick, index } = this.props;

    onClick(index);
  }

  render() {
    const { color, withHover, index } = this.props;
    // if (this.state.isClick) {
    //   // return null;
    // }
    
    return (
      <div className={`oval-holder oval-holder_${color} ${withHover ?
       'oval-holder_is-opacity' : ''}`}
        onClick={this.handleClick}>
        I am {index}          
      </div>
    );
  }
}

