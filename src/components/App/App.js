import { Fragment, Component } from 'react';
import './App.scss';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import { oval } from './constants';
import OvalHolder from '../OvalHolder/OvalHolder';

export default class App extends Component {
  state = {
    arr: oval(),
  }

  handleClick = (index) => {
    const newArr = [...this.state.arr];
    newArr.splice(index, 1);
    console.log(newArr);
    this.setState({arr: newArr});
  }

  render () {
    return (
      <Fragment>
        <Header/>
  
        <main>
          {this.state.arr.map((item, index) => (
            <OvalHolder
              key={index}
              index={index}
              color={item.color}
              withHover={item.withHover} 
              onClick={this.handleClick}
            />
        ))}
        </main>

        <Footer/>
         
      </Fragment> 
    );
  }
}
