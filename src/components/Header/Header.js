import './Header.scss';
import Button from '../Button/Button';

export default function Header({ text, onClick }) {
  return (
    <header className='header'>
      <div className='header__text'>
        текс
      </div>
        <Button text='Кнопка-1'/>
    </header>
  );
}

// // export default function Header({ text }) {
// //   const handleClick = () => {
// //     console.log(text);
// //   };

// //   return (
// //     <header className='header'>
// //       <div>
// //         текс
// //       </div>
// //       <button onClick={handleClick}>{text}</button>
// //     </header>
// //   );
// // }
